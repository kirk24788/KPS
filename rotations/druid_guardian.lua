--[[[
@module Druid Guardian Rotation
@author xvir.subzrk
@version 7.0.3
]]--

local spells = kps.spells.druid
local env = kps.env.druid

--[[
Suggested Talents:
Level 15: Blood Frenzy
Level 30: Guttural Roars
Level 45: Restoration Affinity
Level 60: Mighty Bash
Level 75: Galactic Guardian
Level 90: Guardian of Elune
Level 100: Rend and Tear
]]--

kps.rotations.register("DRUID","GUARDIAN",
{
    {kps.hekili({
    })}
}
,"Hekili")
