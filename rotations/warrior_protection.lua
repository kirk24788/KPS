--[[[
@module Warrior Protection Rotation
@generated_from warrior_protection.simc
@version 8.1.5
]]--
local spells = kps.spells.warrior
local env = kps.env.warrior


kps.rotations.register("WARRIOR","PROTECTION",
{
    {spells.demoralizingShout, 'target.distance <= 10'}, -- charge
    {spells.shieldBlock, 'not player.hasBuff(spells.shieldBlock) and player.rage >= 30'},
    {spells.ignorePain, 'not player.hasBuff(spells.ignorePain) and player.rage >= 40'},
    {spells.thunderClap},
    {spells.shieldSlam},
    {spells.revenge, 'player.hasBuff(spells.revengeFree)'},
    {spells.devastate},
}
,"Easy Modeww")
