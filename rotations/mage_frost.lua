--[[[
@module Mage Frost Rotation
@generated_from mage_frost.simc
@version 7.0.3
]]--
local spells = kps.spells.mage
local env = kps.env.mage


kps.rotations.register("MAGE","FROST",
{
    {kps.hekili({
        spells.counterspell
    }), 'keys.shift'},
    {kps.hekili({
        spells.counterspell,
        spells.blizzard
    })}
}
,"Hekili")
